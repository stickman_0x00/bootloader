# Learn bootloader

Working bootloader with entry to kernel.

## Where to start

If you are someone like me, that doesn't like to go to documentation right away, start by looking at resources.md, Youtube, Practice.

or

src/bootloader/main.asm

## Run

```bash
# build
make
# run
make run
# debug
make debug
```

## Todo

- [ ] Understand why can only boot with floppy disk(-fda) and not an hard disk(-hda)

- [ ] Understand read from disk BIOS interrupt

- [ ] Understand A20 / Comment it better if can

- [ ] Make use of real mode code (src/bootloader/real_mode.asm)