# COLORS
export BLACK		:= $(shell tput -Txterm setaf 0)
export RED			:= $(shell tput -Txterm setaf 1;)
export GREEN		:= $(shell tput -Txterm setaf 2)
export YELLOW		:= $(shell tput -Txterm setaf 3)
export LIGHTPURPLE	:= $(shell tput -Txterm setaf 4)
export PURPLE		:= $(shell tput -Txterm setaf 5)
export BLUE			:= $(shell tput -Txterm setaf 6)
export WHITE		:= $(shell tput -Txterm setaf 7)
export RESET		:= $(shell tput -Txterm sgr0)
# END COLORS

# SOFTWARE
export TARGET := i686-elf-
export GCC := $(TARGET)gcc
export LD := $(TARGET)ld
export ASM := nasm
AS := $(TARGET)as
QEMU := qemu-system-i386
READELF := $(TARGET)readelf
export OBJCOPY := $(TARGET)objcopy
OBJDUMP := $(TARGET)objdump
# END SOFTWARE

# FOLDERS
# Compiled objects
BUILD_DIR = $(abspath build)
export BUILD_KERNEL_DIR = $(BUILD_DIR)/kernel
export BUILD_BOOTLOADER_DIR = $(BUILD_DIR)/bootloader
# Source code
SRC_DIR = $(abspath src)
export SRC_KERNEL_DIR = $(SRC_DIR)/kernel
export SRC_BOOTLOADER_DIR = $(SRC_DIR)/bootloader
# Debug
DEBUG_DIR = $(abspath debug)
export DEBUG_KERNEL_DIR = $(DEBUG_DIR)/kernel
export DEBUG_BOOTLOADER_DIR = $(DEBUG_DIR)/bootloader
# End FOLDERS

# FILES
# Bootloader
export BOOTLOADER_BIN := $(BUILD_BOOTLOADER_DIR)/bootloader.bin
export BOOTLOADER_ELF := $(BUILD_BOOTLOADER_DIR)/bootloader.elf
# Kernel
export KERNEL_BIN := $(BUILD_KERNEL_DIR)/kernel.bin
export KERNEL_ELF := $(BUILD_KERNEL_DIR)/kernel.elf
# Output os file (final product)
OS=my_os
OS_BIN=$(BUILD_DIR)/$(OS).bin
# FILES END


.PHONY: all clean debug always

all: $(OS_BIN) debug_files

always:

# BUILD
# Bootloader build
$(BOOTLOADER_BIN): always
	@$(MAKE) -C $(SRC_BOOTLOADER_DIR)

# Kernel build
$(KERNEL_BIN): always
	@$(MAKE) -C $(SRC_KERNEL_DIR)

# Generat binary.
$(OS_BIN): $(BOOTLOADER_BIN) $(KERNEL_BIN)
	$(info $(RED)[cat]$(RESET) $^ => $@)
	@cat $^ > $@

# Debug files
include debug/debug.mk
# BUILD END


run: $(OS_BIN)
	@$(QEMU) -fda $< &>/dev/null &

debug: $(OS_BIN) debug_files
	@$(QEMU) \
	-fda $< \
	-s -S \
	&>/dev/null &

	gdb \
	-x .gdbinit \
	$(BOOTLOADER_SYM)

clean: clean_bootloader clean_kernel clean_debug_files
	@rm -rf $(OS_BIN)

clean_bootloader:
	@$(MAKE) -C $(SRC_BOOTLOADER_DIR) clean

clean_kernel:
	@$(MAKE) -C $(SRC_KERNEL_DIR) clean