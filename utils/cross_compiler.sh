#!/bin/bash2

# Build GCC Cross-Compiler - https://wiki.osdev.org/GCC_Cross-Compiler
# This script wil automaticly delete the old cross-compiler and build a new one
# IMPORTANT: - This will delete old source code and cross-compiler

# Compilation variables
PREFIX="$HOME/opt/cross"
TARGET=i686-elf
PATH="$PREFIX/bin:$PATH"

# Versions
binutils_version="2.37" # https://ftp.gnu.org/gnu/binutils
gcc_version="11.2.0" # https://ftp.gnu.org/gnu/gcc

# Generate download links for source code
binutils_link="https://ftp.gnu.org/gnu/binutils/binutils-$binutils_version.tar.xz"
gcc_link="https://ftp.gnu.org/gnu/gcc/gcc-$gcc_version/gcc-$gcc_version.tar.xz"

echo Removing old folders
# Delete previous builds
rm -rf $HOME/opt/cross
# Delete source code
rm -rf $HOME/src

# Create fodlers
mkdir -p $HOME/opt/cross
mkdir -p $HOME/src

# Download source code
cd $HOME/src
wget $binutils_link &
wget $gcc_link &
echo "Downloading source code..."
wait

# Extract archives
tar -xf binutils-$binutils_version.tar.xz &
tar -xf gcc-$gcc_version.tar.xz &
echo "Extracting source code..."
wait

# Compile binutils
cd $HOME/src
mkdir build-binutils; cd build-binutils

../binutils-$binutils_version/configure --target=$TARGET --prefix="$PREFIX" --with-sysroot --disable-nls --disable-werror
[ $? -ne 0 ] && echo "fail ../binutils-$binutils_version/configure --target=$TARGET --prefix="$PREFIX" --with-sysroot --disable-nls --disable-werror" && exit
make
[ $? -ne 0 ] && echo "fail make" && exit
make install
[ $? -ne 0 ] && echo "fail make install" && exit

# Compile gcc
cd $HOME/src

# echo The $PREFIX/bin dir _must_ be in the PATH. We did that above.
# which -- $TARGET-as || echo $TARGET-as is not in the PATH
# echo continue...
# read

mkdir build-gcc; cd build-gcc

../gcc-$gcc_version/configure --target=$TARGET --prefix="$PREFIX" --disable-nls --enable-languages=c,c++,go --without-headers
[ $? -ne 0 ] && echo "fail ../gcc-$gcc_version/configure --target=$TARGET --prefix="$PREFIX" --disable-nls --enable-languages=c,c++,go --without-headers" && exit
make all-gcc
[ $? -ne 0 ] && echo "fail make all-gcc" && exit
make all-target-libgcc
[ $? -ne 0 ] && echo "fail make all-target-libgcc" && exit
make install-gcc
[ $? -ne 0 ] && echo "fail make install-gcc" && exit
make install-target-libgcc
[ $? -ne 0 ] && echo "fail make install-target-libgcc" && exit

echo ### FINISHED ### \
echo To use your new compiler simply by invoking $TARGET-gcc, add \$HOME/opt/cross/bin to your PATH by running:
echo 'echo export PATH="$HOME/opt/cross/bin:$PATH" >> .bashrc'
exit
