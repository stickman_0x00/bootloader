; https://wiki.osdev.org/Protected_Mode
[bits 16]
; switch to protect mode
enter_protected_mode:
	cli ; 1 - disable interrupts

	; 2 - enable A20 Line
	call enable_A20

	; 3 - load gdt
	call load_gdt

	; 4 - change last bit of cr0 to 1
	mov eax, cr0
	or al, 1
	mov cr0, eax ; 32 bit mode

	; 5 - far jump to protected mode
	jmp CODE_SEG_32:start_protected_mode

[bits 32]
start_protected_mode:
	; 6 - setup segments registers
	mov ax, 0x10
	mov ds, ax
	mov ss, ax
	mov edx, 0xb8000

	mov ebx, message_32 ; get string pointer
	call print_32_string

	call KERNEL_LOCATION ; gives control to the kernel

halt_32:
	hlt

message_32 db "Protected mode", 0; started", 0