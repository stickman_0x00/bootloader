[bits 16]
load_kernel:
	mov bx, MSG_LOAD_KERNEL
	call print_16_string
	call print_16_nl

	mov bx, KERNEL_LOCATION ; read from disk
	mov dh, 31 ; kernel sectors
	mov dl, [BOOT_DRIVE]
	call disk_load

	ret

MSG_LOAD_KERNEL db "Loading kernel", 0 ;into memory", 0
KERNEL_LOCATION equ 0x1000