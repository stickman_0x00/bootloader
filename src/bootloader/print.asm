[bits 16]
print_16_string:
	pusha ; push ax, cx, dx, bx, sp, bp, si, di

	mov ah, 0x0e ; Switch to teletype Mode.

print_16_string_loop:
	mov al, [bx] ; get char/byte;

	cmp al, 0 ; check if is the end of the string, byte === 0.
	je print_16_string_done ; end of string.

	; print
	int 0x10 ; Call BIOS interrupt.

	inc bx ; Inc position of the string.

	jmp print_16_string_loop ; continue loop

print_16_string_done:
	popa ; pop di, si, bp, sp, bx, dx, cx, ax
	ret


print_16_nl:
	pusha

	mov ah, 0x0e ; Switch to teletype Mo

	mov al, 0x0a ; newline char (Line Feed) "\n"
	int 0x10 ; print

	mov al, 0x0d ; Carriage Return "\r"
	int 0x10

	popa
	ret

print_16_hex:
	pusha

	mov cx, 0

print_16_hex_loop:
	cmp cx, 4 ; loop 4 times
	je print_16_hex_end

	mov ax, dx
	and ax, 0x000f
	add al, 0x30
	cmp al, 0x39
	jle print_16_hex_step2
	add al, 7

print_16_hex_step2:
	mov bx, HEX_OUT + 5
	sub bx, cx  ; index
	mov [bx], al
	ror dx, 4

	; increment index and loop
	add cx, 1
	jmp print_16_hex_loop

print_16_hex_end:
	mov bx, HEX_OUT
	call print_16_string

	popa
	ret

HEX_OUT:
	db '0x0000',0

[bits 32]
print_32_string:
	pusha
	mov edx, VGA_BASE + 320 ; 2 + rows

	mov ah, 0x0f

print_32_string_loop:
	mov al, [ebx] ; get byte from address

	cmp al, 0 ; check if end of string
	je print_32_string_done

	; print
	mov [edx], ax

	inc bx ; Inc position of the string.
	add edx, 2 ; inc position of video mem position
	mov al, [bx] ; get byte from address

	jmp print_32_string_loop ; continue loop

print_32_string_done:
	popa ; pop di, si, bp, sp, bx, dx, cx, ax
	ret

; https://wiki.osdev.org/Text_UI
; VGA mode 3, the linear text buffer is located at address 0xB8000.
VGA_BASE equ 0xb8000