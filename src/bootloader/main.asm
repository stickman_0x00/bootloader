; https://wiki.osdev.org/Bootloader
;[org 0x7c00]
[bits 16]

global _start
_start:

;start_bootloader:

;get_drive_number
	mov [BOOT_DRIVE], dl ; store boot driver number

setup_bootloader_stack:
	xor ax, ax

;set_segment_registers
	mov ds, ax ; set data segment to 0
	mov es, ax ; set extra segment to 0
	mov ss, ax ; set stack segment to 0

;set_stack:
	mov bp, 0x9000 ; set base Pointer (meant for stack frames)
	mov sp, bp ; set stack pointer

;clean_screen:
	mov ah, 0x0
	mov al, 0x3
	int 0x10

	; print boot message
	mov bx, MSG_START ; get string pointer
	call print_16_string
	call print_16_nl

	call load_kernel

	jmp enter_protected_mode

halt_16:
	jmp $


%include "print.asm"
%include "disk.asm"
%include "load.asm"
%include "protected_mode.asm"
%include "gdt.asm"
%include "a20.asm"


[bits 16]

MSG_START db "Bootloader", 0; started", 0

; Bootloader is 512 bytes.
; last 2 bytes are the signature
; This makes the bootloader be recognize by the BIOS.
times 510-($-$$) db 0
db 0x55, 0xaa ; boot signature.



