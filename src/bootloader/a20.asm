; https://wiki.osdev.org/A20_Line
; https://wiki.osdev.org/%228042%22_PS/2_Controller
[bits 16]

enable_A20:
; test whether the A20 address line was already enabled by the BIOS.
; Returns: 0 in ax if the a20 line is disabled (memory wraps around)
;          1 in ax if the a20 line is enabled (memory does not wrap around)
;
is_A20_enabled:
	pushf
	push ds
	push es
	push di
	push si

	xor ax, ax ; ax = 0
	mov es, ax

	not ax ; ax = 0xFFFF
	mov ds, ax

	mov di, 0x0500
	mov si, 0x0510

	; get identifier located at 0000:7DFE (Extended BIOS Data Area)
	mov al, byte [es:di] ; 0:0x0500 = 0x500
	push ax

	; get identifier located at 0xFFFF:7DFE (Extended BIOS Data Area)
	mov al, byte [ds:si] ; 0xFFFF:0x0510 = 0x100500
	push ax
	; When the two values are different it means that the A20 is already enabled
	; otherwise if the values are identical it must be ruled out that this is not by mere chance.
	;	 Therefore the bootsector identifier needs to be changed,
	;	 for instance by rotating it left by 8 bits
	;, and again compared to the 16 bits word at FFFF:7E0E.
	; When they are still the same
	;	then the A20 address line is disabled otherwise it is enabled.

	mov byte [es:di], 0x00 ;
	mov byte [ds:si], 0xFF

	; comparing indentifier identifier (0xAA55)
	cmp byte [es:di], 0xFF

	pop ax
	mov byte [ds:si], al

	pop ax
	mov byte [es:di], al

    mov ax, 0
    je check_A20

    mov ax, 1

; Purpose: to check the status of the a20 line in a completely self-contained state-preserving way.
;          The function can be modified as necessary by removing push's at the beginning and their
;          respective pop's at the end if complete self-containment is not required.
;
check_A20:
	pop si
	pop di
	pop es
	pop ds
	popf

	cmp ax, 0
	je enable_A20_continue

	ret
;
enable_A20_continue:
	; Disable keyboard
	; Prevent any interruption.
	call wait_input ; Check if we can send a command.
	mov al, ps2_cmd_disable_kb ; send command
	out ps2_contr_command, al


	; Read result of command.
	call wait_input
	mov al, ps2_cmd_read_output
	out ps2_contr_command, al

	; Get result of command.
	call wait_output
	in al, ps2_contr_data
	push eax

	; Write to output port.
	call wait_input
	mov al, ps2_cmd_write_output
	out ps2_contr_command, al

	; Send value to output port
	call wait_input
	pop eax
	or al, 2 ; al || 0b10 - Bit to enable A20 gate
	out ps2_contr_data, al ; Enable A20

	; Enable keyboard
	call wait_input
	mov al, ps2_cmd_enable_kb ; send command
	out ps2_contr_command, al

	call wait_input

	ret

; wait for command to be processed
wait_output:
	; wait until status bit 1 (output buffer) is 1 so it can be read
	in al, ps2_contr_command
	test al, 1 ; al & 0b1
	jz wait_output

	; al == 1
	ret
; wait result of command controller
wait_input:
	; wait until status bit 2 (input buffer) is 0
	; by reading from command port, we read status byte
	in al, ps2_contr_command
	test al,2 ; al & 0b10
	jnz wait_input

	; al == 0b0x
	ret

; PS/2 Controllers
; Data Port
ps2_contr_data			equ 0x60
; Command Register
ps2_contr_command		equ 0x64

; Commands of PS/2 Controllers
; Disable first PS/2 port, keyboard.
ps2_cmd_disable_kb		equ 0xAD
; Enable first PS/2 port, keyboard.
ps2_cmd_enable_kb		equ 0xAE
; Read Controller Output Port.
ps2_cmd_read_output		equ 0xD0
; Write byte to Controller Output Port.
ps2_cmd_write_output	equ 0xD1
