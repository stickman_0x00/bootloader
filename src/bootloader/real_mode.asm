; https://wiki.osdev.org/Real_Mode#Switching_from_Protected_Mode_to_Real_Mode
[bits 32]
; go back to real mode
enter_real_mode:
	jmp word CODE_SEG_16:enter_real_mode_continue ; 1 - jump to 16-bit protected mode segment

[bits 16]
enter_real_mode_continue:
	; 2 - disable protected mode bit in cr0
	mov eax, cr0
	and al, ~1
	mov cr0, eax

	; 3 - jump to real mode
	jmp word 0x00:start_real_mode


start_real_mode:
	; 4 - setup segments
	mov ax, 0
	mov ds, ax
	mov ss, ax

	; 5 - enable interrupts
	sti

	mov bx, MSG_REAL_MODE
	call print_16_string

MSG_REAL_MODE db "Real mode started", 0