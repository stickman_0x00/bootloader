[bits 16]
disk_load:
	pusha

	push dx ; kernel sectors + [BOOT_DRIVE]

	; CHS
	; prepare data for reading the disk
	; al = number of sectors to read (1 - 128)
	; ch = track/cylinder number
	; dh = head number
	; cl = sector number
	mov al, dh ; number of sectors to read
	mov cl, 0x02 ; start from sector 2
				 ; (as sector 1 is our boot sector)
	mov dh, 0x00 ; header number
	mov ch, 0x00 ; track/cylinder number

	; dl = drive number is set as input to disk_load
	; es:bx = buffer pointer is set as input as well

	mov ah, 0x02 ; BIOS read from disk routine ; read mode
	int 0x13 ; read from disk interrupt
	jc disk_error ; jump to error if Carry Flag HIGH

	pop dx ; get back original number of sectors to read
	cmp al, dh ; BIOS sets 'al' to the # of sectors actually read
			   ; compare it to 'dh' and error out if they are !=
	jne sectors_error

	popa
	ret

disk_error:
	mov bx, DISK_ERROR ; print message
	call print_16_string
	call print_16_nl

	mov dh, ah ; get error code
	call print_16_hex ; print error code

	jmp halt_16

sectors_error:
	mov bx, SECTORS_ERROR ; print message
	call print_16_string

	jmp halt_16

DISK_ERROR db "Disk", 0 ; read error", 0
SECTORS_ERROR db "Incorrect", 0 ; number of sectors read", 0
BOOT_DRIVE db 0