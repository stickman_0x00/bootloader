[bits 16]
load_gdt:
	lgdt [GDT_descriptor]

	ret

GDT_start:
	null_descriptor:
		dq 0
	; 32-bit code segment
	code_descriptor_32:
		; Limit
		dw 0xffff ; limit (bits 0-15) = 0xFFFFF for full 32-bit range
		; Base
		dw 0 ; base (bits 0-15) = 0x0
		db 0 ; base (bits 16-23)
		; Access Byte
		db 0b10011010 ; access(pres, priv, type, exe, conf, read, acc)
		; Flags + limit
		db 0b11001111 ; gran, 32 bits, 0, 0, limit
		; Base
		db 0
	; 32-bit data segment
	data_descriptor_32:
		; Limit
		dw 0xffff
		; Base
		dw 0
		db 0
		; Access Byte
		db 0b10010010
		; Flags + limit
		db 0b11001111
		; Base
		db 0
	; 16-bit code segment
	code_descriptor_16:
	dw 0xffff                   ; limit (bits 0-15) = 0xFFFFF
	dw 0                        ; base (bits 0-15) = 0x0
	db 0                        ; base (bits 16-23)
	db 0b10011010                ; access (present, ring 0, code segment, executable, direction 0, readable)
	db 0b00001111                ; granularity (1b pages, 16-bit pmode) + limit (bits 16-19)
	db 0                        ; base high
	; 16-bit data segment
	data_descriptor_16:
	dw 0xffff                   ; limit (bits 0-15) = 0xFFFFF
	dw 0                        ; base (bits 0-15) = 0x0
	db 0                        ; base (bits 16-23)
	db 0b10010010                ; access (present, ring 0, data segment, executable, direction 0, writable)
	db 0b00001111                ; granularity (1b pages, 16-bit pmode) + limit (bits 16-19)
	db 0                        ; base high
GDT_end:

GDT_descriptor:
	dw GDT_end - GDT_start - 1 ; size
	dd GDT_start ; start

CODE_SEG_32 equ code_descriptor_32 - GDT_start
DATA_SEG_32 equ data_descriptor_32 - GDT_start
CODE_SEG_16 equ code_descriptor_16 - GDT_start
DATA_SEG_16 equ data_descriptor_16 - GDT_start