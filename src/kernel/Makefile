ASM_FLAGS := -f elf
C_FLAGS = -std=gnu99 -ffreestanding -O2 -Wall -Wextra -g
LD_FLAGS = -Ttext 0x1000 -Map=$(KERNEL_MAP)

KERNEL_ASM := $(shell find $(SRC_KERNEL_DIR) -name '*.asm')
KERNEL_O = $(KERNEL_ASM:$(SRC_KERNEL_DIR)%.asm=$(BUILD_KERNEL_DIR)%_asm.o)

KERNEL_C := $(shell find $(SRC_KERNEL_DIR) -name '*.c')
KERNEL_O += $(KERNEL_C:$(SRC_KERNEL_DIR)%.c=$(BUILD_KERNEL_DIR)%_c.o)

.PHONY: all clean

all: log \
	$(KERNEL_BIN)

log:
	@$(info $(YELLOW)Kernel$(RESET))

$(KERNEL_BIN): $(KERNEL_ELF)
	@mkdir -p $(@D)

	@$(info $(RED)[objcopy]$(RESET) $^ => $@)
	@$(OBJCOPY) -O binary $< $@

$(KERNEL_ELF): $(KERNEL_O)
	@mkdir -p $(@D)
	@mkdir -p $(DEBUG_KERNEL_DIR)

	@$(info $(RED)[ld]$(RESET) $^ => $@)
	@$(LD) $(LD_FLAGS) -o $@ $^

$(BUILD_KERNEL_DIR)%_c.o: $(KERNEL_C)
	@mkdir -p $(@D)
	@$(info $(RED)[gcc]$(RESET) $< => $@)
	@$(GCC) $(C_FLAGS) -c $< -o $@

$(BUILD_KERNEL_DIR)%_asm.o: $(KERNEL_ASM)
	@mkdir -p $(@D)

	@$(info $(RED)[asm]$(RESET) $< => $@)
	@$(ASM) $(ASM_FLAGS) $< -o $@

clean:
	@rm -rf \
	$(KERNEL_O) \
	$(KERNEL_BIN) \
	$(KERNEL_ELF)