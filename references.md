# MANUALS/BOOKS/PDFS

# Intel® 64 and IA-32 Architectures Software Developer’s Manual

- 9.9.1 Switching to Protected Mode
- 9.9.2 Switching Back to Real-Address Mode

# Youtube

## Practice

- https://www.youtube.com/watch?v=9t-SPC7Tczc&list=PLFjM7v6KGMpjWYxnihhd4P3G0BhMB9ReG
- https://www.youtube.com/watch?v=FkrpUaGThTQ
- https://www.youtube.com/watch?v=MwPjvJ9ulSc&list=PLm3B56ql_akNcvH8vvJRYOc7TbYhRs19M
- https://www.youtube.com/channel/UCRWXAQsN5S3FPDHY4Ttq1Xg

#### Inspiration

- https://www.youtube.com/channel/UC0yLxd3lqRdKS_mWL8Dzp8A
- https://www.youtube.com/c/AndreasKling

## Theory

- https://www.youtube.com/channel/UCseUQK4kC3x2x543nHtGpzw

# Git

- https://github.com/chibicitiberiu/nanobyte_os
- https://github.com/chibicitiberiu/nanobyte_experiments
- https://github.com/mell-o-tron/MellOs
- https://github.com/ltgr/guess-the-number
- https://github.com/davidcallanan/os-series

# Random

## BIOS

- [BIOS int 0x13](https://stanislavs.org/helppc/int_13.html)
- [BIOS disk_load 1 troubleshooting](https://stackoverflow.com/questions/47482308/usb-hard-disk-emulation-cause-a-disk-read-to-fail-bios-int-13)
- [BIOS disk_load 2 troubleshooting](https://stackoverflow.com/questions/32701854/boot-loader-doesnt-jump-to-kernel-code/32705076#32705076)
- [BIOS 0x13/AH=0x02](http://www.ctyme.com/intr/rb-0607.htm)
- [BIOS DISK - GET STATUS OF LAST OPERATION](http://www.ctyme.com/intr/rb-0606.htm#Table234)


- https://dev.to/frosnerd/writing-my-own-boot-loader-3mld
- [Bootloader debug](https://stackoverflow.com/questions/32955887/how-to-disassemble-16-bit-x86-boot-sector-code-in-gdb-with-x-i-pc-it-gets-tr)
- https://silo.tips/download/projeto-1-bootloader

## Assembly

- https://www.bencode.net/blob/nasmcheatsheet.pdf
- https://stanislavs.org/helppc/idx_assembler.html