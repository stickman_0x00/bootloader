# FILES
# kernel
KERNEL_SYM := $(DEBUG_KERNEL_DIR)/kernel.sym
KERNEL_TXT := $(DEBUG_KERNEL_DIR)/kernel.txt
KERNEL_DATA := $(DEBUG_KERNEL_DIR)/kernel.data
export KERNEL_MAP := $(DEBUG_KERNEL_DIR)/kernel.map

# bootloader
BOOTLOADER_SYM := $(DEBUG_BOOTLOADER_DIR)/bootloader.sym
BOOTLOADER_TXT := $(DEBUG_BOOTLOADER_DIR)/bootloader.txt
BOOTLOADER_DATA := $(DEBUG_BOOTLOADER_DIR)/bootloader.data
export BOOTLOADER_MAP := $(DEBUG_BOOTLOADER_DIR)/bootloader.map
# END FILES

debug_files: debug_log debug_bootloader debug_kernel

debug_log:
	@$(info $(YELLOW)Debug$(RESET))

debug_bootloader: \
	$(BOOTLOADER_DATA) \
	$(BOOTLOADER_SYM) \
	$(BOOTLOADER_TXT) \

debug_kernel: \
	$(KERNEL_DATA) \
	$(KERNEL_SYM) \
	$(KERNEL_TXT)

$(BOOTLOADER_DATA): $(BOOTLOADER_ELF)
	$(info $(RED)[objdump]$(RESET) $< => $@)
	@$(OBJDUMP) -s -j .data $< > $@ 2>/dev/null ; true

$(KERNEL_DATA): $(KERNEL_ELF)
	$(info $(RED)[objdump]$(RESET) $< => $@)
	@$(OBJDUMP) -s -j .data $< > $@ 2>/dev/null ; true

$(BOOTLOADER_SYM): $(BOOTLOADER_ELF)
	$(info $(RED)[objcopy]$(RESET) $< => $@)
	@$(OBJCOPY) $< $@

$(KERNEL_SYM): $(KERNEL_ELF)
	$(info $(RED)[objcopy]$(RESET) $< => $@)
	@$(OBJCOPY) $< $@

$(BOOTLOADER_TXT): $(BOOTLOADER_ELF)
	$(info $(RED)[objdump]$(RESET) $< => $@)
	@$(OBJDUMP) -drS $< > $@

$(KERNEL_TXT): $(KERNEL_ELF)
	$(info $(RED)[objdump]$(RESET) $< => $@)
	@$(OBJDUMP) -drS $< > $@


clean_debug_files:
	@rm -rf \
	$(BOOTLOADER_DATA) \
	$(KERNEL_DATA) \
	$(BOOTLOADER_SYM) \
	$(KERNEL_SYM) \
	$(BOOTLOADER_TXT) \
	$(KERNEL_TXT) \
	$(BOOTLOADER_MAP) \
	$(KERNEL_MAP)